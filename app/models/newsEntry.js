//newsEntry.js

var mongoose = require('mongoose');


var newsEntrySchema = mongoose.Schema({
    title       : String,
    desc     	: String,
    createdBy   : String
});


module.exports = mongoose.model('NewsEntry', newsEntrySchema);