module.exports = function(app, passport) {
    var NewsEntry  = require('../app/models/newsEntry');

    //HOME
    app.get('/', function(req, res) {
        res.render('index', { 
            user : req.user,
            title: 'Cambridge News', 
            header: 'Welcome to Cambridge News',
            homenav: 'homenav'

        });
    })

    //LOGIN
    .get('/login', function(req, res) {
        res.render('login', { 
            user : req.user,
            title: 'Cambridge News - User Login',
            message: req.flash('loginMessage'),
            header: 'LOGIN',
            loginnav: 'loginnav'
        });
    }) .post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', 
        failureRedirect : '/login',
        failureFlash : true 
    }))

    //SIGNUP
    .get('/signup', function(req, res) {
        res.render('signup', { 
            title: 'Cambridge News - User Signup',
            message: req.flash('signupMessage'),
            header: 'SIGNUP',
            signupnav: 'signupnav'
        });
    }).post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/profile',
        failureRedirect : '/signup', 
        failureFlash : true
    }))

    //PROFILE
    .get('/profile', isLoggedIn, function(req, res) {
        res.render('profile', {
            user : req.user, 
            title: 'Cambridge News - Profile Page',
            header: 'Welcome',
            profilenav: 'profilenav' 
        });
    })

    //LOGOUT
    .get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    })

    //CREATE
    .get('/create', isLoggedIn, function(req, res) {
        res.render('create', {
            user : req.user, 
            title: 'Cambridge News - Create News',
            header: 'Create News',
            message: req.flash('createMessage'),
            createnav: 'createnav' 
        });
    }).post('/create', isLoggedIn, function(req, res) {


        var title = req.body.title;
        var desc = req.body.desc;

        console.log("title: " + title);
        console.log("desc: " + desc);

        var obj = new NewsEntry({
            title: title,
            desc: desc
        });

        obj.save(function(error){
            if(error){
                console.log(error);
                req.flash('createMessage', 'ERROR!.');
                res.redirect("/create");
            }else{
                console.log("Saved one entry!");
                res.redirect("/view");
            }

        });
    })


    //VIEW
    .get('/view', function(req, res) {
       NewsEntry.find({}, function(error, docs){
            if(error){
                req.flash('viewMessage', 'Retrieval Error. Please try again!');
                res.render('view', { 
                    user        : req.user,
                    title       : 'Cambridge News - View News',
                    header      : 'Today\'s Headlines' ,
                    messages    : req.flash('viewMessage'),
                    viewnav     : 'viewnav' 
                });
            }else{

                res.render('view', { 
                    user        : req.user,
                    title       : 'Cambridge News - View News',
                    header      : 'Today\'s Headlines' ,
                    messages    : req.flash('viewMessage'),
                    newsList    : docs,
                    viewnav     : 'viewnav' 
                });
        
            }
        });

    })
  

    //FB ROUTES
    .get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }))

    .get('/auth/facebook/callback',
        passport.authenticate('facebook',  {
            successRedirect : '/profile',
            failureRedirect : '/'
     })
    )

    //GOOGLE ROUTES
    .get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }))

    .get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect : '/profile',
            failureRedirect : '/'
    }));

    //LOGIN CHECK
    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();

        res.redirect('/');
    }
}